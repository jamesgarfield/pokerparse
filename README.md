# Problem statement

Poker is a game played with a standard 52-card deck of cards (https://en.wikipedia.org/wiki/Standard_52-card_deck), in which players attempt to make the best possible 5-card hand according to the ranking of the categories given at the following site:  http://www.pokerlistings.com/poker-hand-ranking. If you are unfamiliar with poker we recommend that you familiarize yourself with this list. The provided link also has a short video explaining how these hands work.


In this challenge, you may assume:

A single 52 card deck will be in use

No wild cards

Aces are treated as high cards only


Cards will be represented by their number or first letter for the non-numeric cards (J, Q, K, A) and the suits will be represented by their first letter (H, C, D, S) and stored as a JSON array. So for example a hand J♥ 4♣ 4♠ J♣ 9♥ will be represented as ["JH", "4C", "4S", "JC", "9H"] . 


When a category involves less than 5 cards, the next highest cards are added as “kickers” for the sake of breaking ties.  For example, a pair of queens with a king beats a pair of queens with a 10.


1. Write a function that takes a 5-card hand as a JSON array and determines its category, with any tie-breaking information that is necessary.  For example, the input  ["JH", "4C", "4S", "JC", "9H"] would have the value of two pair: jacks and 4s with a 9 kicker. You may choose your own representation for the output.



2. Write a function that takes 2 or more 5-card hands and determines the winner.



3. Some poker variations use more than 5 cards per player, and the player chooses the best subset of 5 cards to play. Write a function that takes 5 or more cards and returns the best 5-card hand that can be made with those cards.  For example, the input ["3H", "7S", "3S", "QD", "AH", "3D", "4S"] should return [“3H”, “3S”, “3D”, “AH”, “QD”], which is a 3-of-a-kind with 3s, ace and queen kickers.




# Poker Utils

To run/build, set your GOPATH to the `go` subdirectory

Example CLI invocations from the precompiled versions in the `/bin` directory

`pokercmd score "[\"9H\",\"9D\",\"9C\",\"3S\",\"5H\"]"`

`pokercmd winner "[\"9H\",\"9D\",\"9C\",\"3S\",\"5H\"]" "[\"2D\",\"3D\",\"4D\",\"6D\",\"JD\"]"`

`pokercmd pick "[\"2D\",\"3D\",\"4D\",\"6D\",\"JD\", \"JS\", \"JH\"]"`

Running from source is similar, just prefix with `go run` and the path to the main program, e.g.

`go run ./go/src/poker/pokercmd score "[\"9H\",\"9D\",\"9C\",\"3S\",\"5H\"]"`
