package poker

import (
	"fmt"
	"testing"
)

func Test_kicker(t *testing.T) {
	tests := []struct {
		ranks  []int
		expect int
	}{
		{[]int{2}, 2},
		{[]int{14}, 14},
		{[]int{13, 2}, 1302},
		{[]int{12, 11}, 1211},
		{[]int{5, 2}, 502},
		{[]int{14, 2, 3}, 140302},
		{[]int{13, 12, 11}, 131211},
		{[]int{14, 13, 12}, 141312},
		{[]int{4, 3, 2}, 40302},
	}
	for i, test := range tests {
		t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
			k := stackRanks(test.ranks...)
			if k != test.expect {
				t.Fatalf("expected %d, got %d", test.expect, k)
			}
		})
	}
}

func Test_scoreHand(t *testing.T) {
	tests := []struct {
		name   string
		hand   []string
		expect Score
	}{
		{"highcard", []string{"9H", "2C", "JD", "10S", "5C"}, Score{HighCard, 11, 10090502}},
		{"pair", []string{"2C", "3C", "KH", "2D", "8S"}, Score{Pair, 2, 130803}},
		{"2pair", []string{"QD", "3C", "5S", "3D", "QS"}, Score{TwoPair, 1203, 5}},
		{"trips", []string{"2S", "2H", "AC", "2D", "KS"}, Score{Trips, 2, 1413}},
		{"straight", []string{"KH", "JD", "10H", "9C", "QS"}, Score{Straight, 13, 0}},
		{"flush", []string{"KH", "2H", "10H", "9H", "4H"}, Score{Flush, 13, 0}},
		{"full house", []string{"KH", "2H", "KC", "KD", "2C"}, Score{FullHouse, 13, 2}},
		{"quads", []string{"KH", "2H", "KC", "KD", "KS"}, Score{Quads, 13, 2}},
		{"straightflush", []string{"KH", "AH", "QH", "JH", "10H"}, Score{StraightFlush, 14, 0}},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			cards := make([]Card, 5)
			for i, card := range test.hand {
				c, err := parseCard([]byte(card))
				if err != nil {
					t.Fatalf("parsing error: %v for card %s", err, card)
				}
				cards[i] = c
			}
			score := ScoreHand(newHand(cards...))
			if score != test.expect {
				t.Fatalf("expected %+v, got %+v", test.expect, score)
			}
		})
	}
}
