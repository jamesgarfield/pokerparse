package poker

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
)

var (
	faceToValue = map[string]int{
		"J": Jack,
		"Q": Queen,
		"K": King,
		"A": Ace,
	}
	valueToFace = map[int]string{
		Jack:  "J",
		Queen: "Q",
		King:  "K",
		Ace:   "A",
	}
)

var (
	ErrInvalidCard = errors.New("invalid card")
)

func (c Card) String() string {
	if face, ok := valueToFace[c.Rank]; ok {
		return fmt.Sprintf("%s%s", face, c.Suit)
	}
	return fmt.Sprintf("%d%s", c.Rank, c.Suit)
}

func (c *Card) UnmarshalJSON(b []byte) error {
	card, err := parseCard(b[1 : len(b)-1])
	if err != nil {
		return fmt.Errorf("%s: %v", b, err)
	}
	*c = card
	return nil
}

func (h Hand) String() string {
	s := make([]string, 5)
	for i, c := range h.cards {
		s[i] = c.String()
	}
	return fmt.Sprintf("%v", s)
}

func (h *Hand) UnmarshalJSON(b []byte) error {
	cards := []Card{}
	if err := json.Unmarshal(b, &cards); err != nil {
		return err
	}
	// ensure that we have a canonical, sorted representation
	hand, err := NewHand(cards...)
	if err != nil {
		return err
	}
	*h = hand
	return nil
}

func parseCard(b []byte) (Card, error) {
	rank, err := parseRank(b)
	if err != nil {
		return Card{}, err
	}

	suit, err := parseSuit(b)
	if err != nil {
		return Card{}, err
	}

	return Card{rank, suit}, nil
}

func parseRank(b []byte) (int, error) {
	var token string
	switch len(b) {
	case 2:
		token = string(b[0:1])
	case 3:
		token = string(b[0:2])
	default:
		return 0, ErrInvalidCard
	}

	if faceVal, ok := faceToValue[token]; ok {
		return faceVal, nil
	}

	value, err := strconv.Atoi(token)
	if err != nil {
		return 0, ErrInvalidCard
	}

	if value < 2 || value > 10 {
		return 0, ErrInvalidCard
	}

	return value, nil
}

func parseSuit(b []byte) (Suit, error) {
	token := b[len(b)-1:]

	switch s := Suit(token); s {
	case Diamonds, Clubs, Hearts, Spades:
		return s, nil
	default:
		return Suit(""), ErrInvalidCard
	}
}
