package poker

import (
	"reflect"
	"testing"
)

func Test_PickHand(t *testing.T) {
	tests := []struct {
		name   string
		cards  []Card
		expect []Card
	}{
		{
			"email sample",
			[]Card{{3, Hearts}, {7, Spades}, {3, Spades}, {Ace, Hearts}, {Queen, Diamonds}, {3, Diamonds}, {4, Spades}},
			[]Card{{3, Hearts}, {3, Spades}, {3, Diamonds}, {Ace, Hearts}, {Queen, Diamonds}},
		},
		{
			"highcard",
			[]Card{{9, Hearts}, {2, Clubs}, {Jack, Diamonds}, {10, Spades}, {5, Clubs}, {4, Clubs}, {3, Hearts}},
			[]Card{{9, Hearts}, {Jack, Diamonds}, {10, Spades}, {5, Clubs}, {4, Clubs}},
		},
		{
			"pair",
			[]Card{{2, Clubs}, {3, Clubs}, {King, Hearts}, {2, Diamonds}, {8, Spades}, {Ace, Clubs}, {7, Hearts}},
			[]Card{{2, Clubs}, {King, Hearts}, {2, Diamonds}, {8, Spades}, {Ace, Clubs}},
		},
		{
			"2pair",
			[]Card{{Queen, Diamonds}, {3, Clubs}, {5, Spades}, {3, Diamonds}, {Queen, Spades}, {2, Clubs}, {4, Spades}},
			[]Card{{Queen, Diamonds}, {3, Clubs}, {5, Spades}, {3, Diamonds}, {Queen, Spades}},
		},
		{
			"trips",
			[]Card{{2, Spades}, {2, Hearts}, {Ace, Clubs}, {2, Diamonds}, {King, Spades}, {3, Diamonds}, {4, Clubs}},
			[]Card{{2, Spades}, {2, Hearts}, {Ace, Clubs}, {2, Diamonds}, {King, Spades}},
		},
		{
			"straight",
			[]Card{{King, Hearts}, {Jack, Diamonds}, {10, Hearts}, {9, Clubs}, {Queen, Spades}, {5, Clubs}, {2, Clubs}},
			[]Card{{King, Hearts}, {Jack, Diamonds}, {10, Hearts}, {9, Clubs}, {Queen, Spades}},
		},
		{
			"flush",
			[]Card{{King, Hearts}, {King, Diamonds}, {King, Clubs}, {2, Hearts}, {10, Hearts}, {9, Hearts}, {5, Hearts}},
			[]Card{{King, Hearts}, {2, Hearts}, {10, Hearts}, {9, Hearts}, {5, Hearts}},
		},
		{
			"full house",
			[]Card{{King, Hearts}, {2, Hearts}, {King, Clubs}, {4, Spades}, {2, Diamonds}, {King, Diamonds}, {Jack, Clubs}},
			[]Card{{King, Hearts}, {2, Hearts}, {King, Clubs}, {2, Diamonds}, {King, Diamonds}},
		},
		{
			"quads",
			[]Card{{King, Hearts}, {Jack, Hearts}, {King, Clubs}, {King, Diamonds}, {King, Spades}, {Ace, Spades}, {Queen, Diamonds}},
			[]Card{{King, Hearts}, {King, Clubs}, {King, Diamonds}, {King, Spades}, {Ace, Spades}},
		},
		{
			"straightflush",
			[]Card{{King, Hearts}, {Ace, Diamonds}, {Ace, Hearts}, {King, Diamonds}, {Queen, Hearts}, {Jack, Hearts}, {10, Hearts}},
			[]Card{{King, Hearts}, {Ace, Hearts}, {Queen, Hearts}, {Jack, Hearts}, {10, Hearts}},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			hand, err := PickHand(test.cards)
			if err != nil {
				t.Fatalf("unexpected error: %+v", err)
			}
			if hand != newHand(test.expect...) {
				t.Fatalf("expected %+v, got %+v", test.expect, hand)
			}
		})
	}
}

func Test_combinations(t *testing.T) {
	combos := combinations(5)
	expect := [][]int{{0, 1, 2, 3, 4}}
	if !reflect.DeepEqual(expect, combos) {
		t.Fatalf("expected: %+v, got %+v", expect, combos)
	}

	combos = combinations(6)
	expect = [][]int{
		{0, 1, 2, 3, 4},
		{0, 1, 2, 3, 5},
		{0, 1, 2, 4, 5},
		{0, 1, 3, 4, 5},
		{0, 2, 3, 4, 5},
		{1, 2, 3, 4, 5},
	}
	if !reflect.DeepEqual(expect, combos) {
		t.Fatalf("expected: %+v, got %+v", expect, combos)
	}
}
