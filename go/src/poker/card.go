package poker

import (
	"errors"
	"sort"
)

type Suit string

const (
	Clubs    Suit = "C"
	Diamonds Suit = "D"
	Hearts   Suit = "H"
	Spades   Suit = "S"
)

const (
	Jack  = 11
	Queen = 12
	King  = 13
	Ace   = 14
)

type Card struct {
	Rank int
	Suit
}

func (c Card) Less(x Card) bool {
	if c.Rank == x.Rank {
		return c.Suit < x.Suit
	}
	return c.Rank < x.Rank
}

type Hand struct {
	// hide the internal representation of cards in a Hand to force
	// the useage of the constructor by external consumers to ensure
	// that canonical hands are always sorted according to the
	// expected sort order, minimizing the number of sorts across
	// all operations
	// represent as a static array (rather than a slice) so that
	// direct structural comparisons can be done for equality rather
	// than needing to write an Equals function that loops
	cards [5]Card
}

var ErrInvalidHand = errors.New("invalid hand")

func NewHand(cards ...Card) (Hand, error) {
	if len(cards) != 5 {
		return Hand{}, ErrInvalidHand
	}
	return newHand(cards...), nil
}

func newHand(cards ...Card) Hand {
	sort.SliceStable(cards, func(i, j int) bool {
		ci := cards[i]
		cj := cards[j]
		return ci.Less(cj)
	})
	return Hand{[5]Card{cards[0], cards[1], cards[2], cards[3], cards[4]}}
}

func (h Hand) Cards() []Card {
	return h.cards[:]
}
