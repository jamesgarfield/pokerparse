package poker

type Winner int

const (
	NoWinner Winner = iota
	LeftHand
	RightHand
)

func CompareHands(left, right Hand) Winner {
	return CompareScores(ScoreHand(left), ScoreHand(right))
}

func CompareScores(l, r Score) Winner {

	if l.HandRank > r.HandRank {
		return LeftHand
	} else if l.HandRank < r.HandRank {
		return RightHand
	}

	if l.RelativeScore > r.RelativeScore {
		return LeftHand
	} else if l.RelativeScore < r.RelativeScore {
		return RightHand
	}

	if l.Kicker > r.Kicker {
		return LeftHand
	} else if l.Kicker < r.Kicker {
		return RightHand
	}

	return NoWinner
}
