package poker

import (
	"errors"
	"sort"
)

type HandScore struct {
	Hand
	Score
}

var (
	ErrMinPick = errors.New("must pick from at least 5 cards")
)

// Pick the best 5-card hand of 5 or more cards
func PickHand(cards []Card) (Hand, error) {
	if len(cards) < 5 {
		return Hand{}, ErrMinPick
	}
	scores := []HandScore{}
	combos := handCombinations(cards)
	c := make(chan HandScore)

	for _, hand := range combos {
		go func(h Hand) {
			score := ScoreHand(h)
			c <- HandScore{h, score}
		}(hand)
	}

	for len(scores) < len(combos) {
		scores = append(scores, <-c)
	}

	sort.Slice(scores, func(i, j int) bool {
		return CompareScores(scores[i].Score, scores[j].Score) == LeftHand
	})

	return scores[0].Hand, nil
}

func handCombinations(cards []Card) []Hand {
	hands := []Hand{}

	combos := combinations(len(cards))
	for _, combo := range combos {
		handCards := make([]Card, 5)
		for i, index := range combo {
			card := cards[index]
			handCards[i] = card
		}
		hand := newHand(handCards...)
		hands = append(hands, hand)

	}
	return hands
}

// don't need a generalized r-of-n combinations producer
// can have simpler code by special casing for r=5
// while still allowing for any n >= 5
func combinations(n int) [][]int {
	combos := [][]int{}
	for a := 0; a < n-4; a++ {
		for b := a + 1; b < n-3; b++ {
			for c := b + 1; c < n-2; c++ {
				for d := c + 1; d < n-1; d++ {
					for e := d + 1; e < n; e++ {
						combos = append(combos, []int{a, b, c, d, e})
					}
				}
			}
		}
	}
	return combos
}
