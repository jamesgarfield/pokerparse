package poker

import (
	"fmt"
	"math"
	"sort"
)

// Each type of scored poker hand (e.g. Pair, Full House) is represented
// by its own HandRank.
// Royal Flush is just a degenerate case of Staight Flush having the highest
// relative score among striaght flushes and doesn't require it's own  ranking
type HandRank int

const (
	HighCard HandRank = iota
	Pair
	TwoPair
	Trips
	Straight
	Flush
	FullHouse
	Quads
	StraightFlush
)

// Score is an orderable by HandRank scoring structure for a 5-card
// poker hand.
// RelativeScore is a non-absolute score value for a given HandRank
// (i.e. not comparable between different HandRanks) that can be used
// to determine the higher of two equal HandRanks, while the Kicker
// value acts as a fall-back, also non-absolute tie-breaker
type Score struct {
	HandRank
	RelativeScore int
	Kicker        int
}

func (s Score) String() string {
	switch s.HandRank {
	case HighCard:
		return fmt.Sprintf("High Card")
	case Pair:
		return fmt.Sprintf("Pair")
	case TwoPair:
		return fmt.Sprintf("Two Pair")
	case Trips:
		return fmt.Sprintf("Trips")
	case Straight:
		return fmt.Sprintf("Stright")
	case Flush:
		return fmt.Sprintf("Flush")
	case FullHouse:
		return fmt.Sprintf("Full House")
	case Quads:
		return fmt.Sprintf("Quad")
	case StraightFlush:
		return fmt.Sprintf("Straight Flush")
	default:
		panic("invalid score")
	}
}

// Provide a scoring of a single 5-card poker hand
func ScoreHand(hand Hand) Score {
	cards := hand.cards

	ranks := map[int][]Card{}
	suits := map[Suit][]Card{}

	for _, c := range cards {
		ranks[c.Rank] = append(ranks[c.Rank], c)
		suits[c.Suit] = append(suits[c.Suit], c)
	}

	isFlush := len(suits) == 1
	isStraight := len(ranks) == 5 && (cards[4].Rank-cards[0].Rank) == 4

	if isFlush && isStraight {
		return Score{HandRank: StraightFlush, RelativeScore: cards[4].Rank}
	} else if isFlush {
		return Score{HandRank: Flush, RelativeScore: cards[4].Rank}
	} else if isStraight {
		return Score{HandRank: Straight, RelativeScore: cards[4].Rank}
	}

	rankCount := len(ranks)
	if rankCount == 2 {
		score := Score{}
		for rank, cards := range ranks {
			switch len(cards) {
			case 4:
				score.HandRank = Quads
				score.RelativeScore = rank
			case 3:
				score.HandRank = FullHouse
				score.RelativeScore = rank
			case 2:
				score.Kicker = rank
			case 1:
				score.Kicker = rank
			}
		}
		return score
	}

	if rankCount == 3 {
		score := Score{}
		rScore := []int{}
		kicker := []int{}
		for rank, set := range ranks {
			switch len(set) {
			case 3:
				score.HandRank = Trips
				rScore = append(rScore, rank)
			case 2:
				score.HandRank = TwoPair
				rScore = append(rScore, rank)
			case 1:
				kicker = append(kicker, rank)
			}
		}
		score.RelativeScore = stackRanks(rScore...)
		score.Kicker = stackRanks(kicker...)
		return score
	}

	if rankCount == 4 {
		score := Score{HandRank: Pair}
		kicker := []int{}
		for rank, set := range ranks {
			switch len(set) {
			case 2:
				score.RelativeScore = rank
			case 1:
				kicker = append(kicker, rank)
			}
		}
		score.Kicker = stackRanks(kicker...)
		return score
	}

	score := Score{HandRank: HighCard, RelativeScore: cards[4].Rank}
	kick := []int{}
	for i := 0; i < 4; i++ {
		kick = append(kick, cards[i].Rank)
	}
	score.Kicker = stackRanks(kick...)
	return score
}

// stack a set of ranks in size order in different orders of magnitude
// allows for a single unit of comparable representaiton of several ranks
// to allow for easy comparison multiple ranks
// Useful for generating kicker values and the score for 2pair
func stackRanks(ranks ...int) int {
	sort.Sort(sort.Reverse(sort.IntSlice(ranks)))
	k := 0
	n := (len(ranks) - 1) * 2
	for i, r := range ranks {
		k += r * int(math.Pow10((n - (i * 2))))
	}
	return k
}
