package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"poker"
)

func main() {
	if len(os.Args) == 1 {
		log.Fatal("must supply a sub-command")
	}
	cmd := os.Args[1]
	args := os.Args[2:]
	switch cmd {
	case "score":
		score(args)
	case "winner":
		winner(args)
	case "pick":
		pick(args)
	default:
		log.Fatal("invalid sub-command. Use [score, winner, pick]")
	}
}

func score(args []string) {
	if len(args) != 1 {
		log.Fatal("must supply a single JSON hand argument")
	}
	hand := poker.Hand{}
	if err := json.Unmarshal([]byte(args[0]), &hand); err != nil {
		log.Fatalf("invalid hand representation: %+v", err)
	}
	score := poker.ScoreHand(hand)
	fmt.Printf("Hand: %v, Score: %s\n", hand, score)
}

func winner(args []string) {
	if len(args) != 2 {
		log.Fatal("must two JSON hand arguments")
	}
	left := poker.Hand{}
	if err := json.Unmarshal([]byte(args[0]), &left); err != nil {
		log.Fatalf("invalid left hand representation: %+v", err)
	}

	right := poker.Hand{}
	if err := json.Unmarshal([]byte(args[1]), &right); err != nil {
		log.Fatalf("invalid right hand representation: %+v", err)
	}

	winner := poker.CompareHands(left, right)
	switch winner {
	case poker.LeftHand:
		score := poker.ScoreHand(left)
		fmt.Printf("Left hand %+v wins with a %s\n", left, score)
	case poker.RightHand:
		score := poker.ScoreHand(right)
		fmt.Printf("Right hand %+v wins with a %s\n", right, score)
	default:
		fmt.Println("Neither hand wins")
	}
}

func pick(args []string) {
	if len(args) != 1 {
		log.Fatal("must supply a single JSON set of cards argument")
	}

	cards := []poker.Card{}
	if err := json.Unmarshal([]byte(args[0]), &cards); err != nil {
		log.Fatalf("invalid cards representation: %+v", err)
	}

	hand, err := poker.PickHand(cards)
	if err != nil {
		log.Fatalf("pick error: %+v", err)
	}
	score := poker.ScoreHand(hand)
	fmt.Printf("Best hand: %v (%s)\n", hand, score)
}
