package poker

import (
	"encoding/json"
	"testing"
)

func Test_parseCard(t *testing.T) {
	tests := []struct {
		s      string
		expect Card
	}{
		{"9H", Card{9, Hearts}},
		{"2C", Card{2, Clubs}},
		{"JS", Card{Jack, Spades}},
		{"AD", Card{Ace, Diamonds}},
		{"10H", Card{10, Hearts}},
	}

	for _, test := range tests {
		card, err := parseCard([]byte(test.s))
		if err != nil {
			t.Fatalf("unexpected error: %v", err)
		}
		if card != test.expect {
			t.Fatalf("expected %s, got %s", test.expect, card)
		}
	}
}

func Test_Hand_UnmarshalJSON(t *testing.T) {
	b := []byte(`["9H", "2D", "2C", "AS", "5C"]`)
	hand := Hand{}
	if err := json.Unmarshal(b, &hand); err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	expect := newHand([]Card{{9, Hearts}, {2, Diamonds}, {2, Clubs}, {Ace, Spades}, {5, Clubs}}...)
	if hand != expect {
		t.Fatalf("expected: %+v, got: %+v", expect, hand)
	}
}
